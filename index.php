<?php
    date_default_timezone_set('Europe/Moscow');
    define('ROOT', $_SERVER['DOCUMENT_ROOT'] . '/'); // получение пути до корня расположения скрипта
    define('DIR_APP', ROOT . 'app/'); // указание дальнейшего пути приложения
    define('DIR_LIBS', DIR_APP . 'libs/'); // указание пути библиотек приложения
    define('DIR_EXT', DIR_APP . 'extensions/');

    include DIR_LIBS . 'MeekroDB/meekrodb.php'; // подключаем библиотеку для работы с бд.
    include DIR_APP . 'config.php'; // конфигурация, важно - подключать после библиотеки бд.
    include DIR_APP . 'functions.php'; // дополнительные функции

    require DIR_LIBS . 'flight/Flight.php'; // подключение библиотеки роутера
    require DIR_LIBS . 'Twig/Autoloader.php'; // подключение библиотеки шаблонизатора

    Twig_Autoloader::register(); // запуск шаблонизатора

    Router::init(ROOT . 'routes/', 'php'); // автозагрузка всех роутов

    Flight::start(); // запуск роутера
?>

<?php
    function ae_system() {
        $sql = new DB;
        $aeConfig = new aeConfig;

        try {
            $loader = new Twig_Loader_Filesystem($aeConfig->site()['theme'] . $aeConfig->site()['theme_views']);
            $twig = new Twig_Environment($loader);
            $twig->addExtension(new Twig_Extension_StringLoader());
            $template = $twig->loadTemplate('system.tpl');

            echo $template->render(array(
                    'title' => $aeConfig->site()['title'],
                    'theme_dir_static' => $aeConfig->site()['theme'] . $aeConfig->site()['theme_static'],
                    'aev' => aev
            ));
        } catch (Exception $e) {
            die ('Error: ' . $e->getMessage());
        }
    }

    Flight::route('/system/', 'ae_system');
?>

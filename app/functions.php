<?php
    class Router {
        public static function init ($dir, $ext) {
            $opendir = opendir($dir);
            while ($file = readdir($opendir)) {
                $c_ext = substr($file, strlen($ext)*-1);
                if (($file != '.') && ($file != '..') && ($c_ext == $ext)) {
                    include ($dir . $file);
                }
            }
            closedir($opendir);
        }

        public static function add ($dir, $file) {
            include ($dir . $file);
        }
    }

    class SpogaCrypt {
        public static function openssl_myrand($length) {
            $min = 0;
            $max = 100;
            $big = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $small = "abcdefghijklmnopqrstuvwxyz";
            $nums = "0123456789";
            $bigA = str_shuffle($big);
            $smallA = str_shuffle($small);
            $numsA = str_shuffle($nums);
            $subA = substr($bigA,0,5);
            $subB = substr($bigA,6,5);
            $subC = substr($bigA,10,5);
            $subD = substr($smallA,0,5);
            $subE = substr($smallA,6,5);
            $subF = substr($smallA,10,5);
            $subG = substr($numsA,0,5);
            $subH = substr($numsA,6,5);
            $subI = substr($numsA,10,5);

            $random1 = str_shuffle($subA . $subD . $subB . $subF . $subC . $subE);
            $random2 = str_shuffle($random1);
            $random = $random1 . $random2;

            if ($length > $min && $length < $max) {
                $code = substr($random, 0, $length);
            } else {
                $code = $random;
            }

            return $code;
        }
    }

    class Redactor {
        public static function clear_tags($str) {
		    return strip_tags($str, '<code><span><div><label><a><br><p><b><i><del><strike><u><img><video><audio><iframe><object><embed><param><blockquote><mark><cite><small><ul><ol><li><hr><dl><dt><dd><sup><sub><big><pre><code><figure><figcaption><strong><em><table><tr><td><th><tbody><thead><tfoot><h1><h2><h3><h4><h5><h6>');
	    }

        public static function translit($str) {
    		$russian = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', ' ', '	');
    		$english = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya', '_', '_');
    		return str_replace($russian, $english, $str);
    	}

        public static function clear_chat_tags($str) {
		    return strip_tags($str, '<img>');
	    }
    }
?>

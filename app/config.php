<?php
    define('aev', 'aLPS Engine 1.1.5');
    define('DOMAIN', $_SERVER['SERVER_NAME']);

    DB::$user = '';
    DB::$password = '';
    DB::$dbName = '';
    DB::$host = ''; //defaults to localhost if omitted
    DB::$port = '3306'; // defaults to 3306 if omitted
    DB::$encoding = 'utf8'; // defaults to latin1 if omitted

    class aeConfig {
        public static function site() {
            $site = Array(
                'title' => '',
                'theme' => 'theme/',
                'theme_views' => 'views',
                'theme_static' => 'static',
                'admin_views' => 'admin',
                'upload_folder' => 'uploads'
            );

            return $site;
        }
    }
?>
